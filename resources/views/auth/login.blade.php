@extends('auth.layout')
@section('contents')
<div class="login-box">
	<div class="login-logo">
		<a href="{{url('/')}}"><b>DASHBOARD </b>LOGIN</a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		@if($errors->has('email'))
			<h6 class="alert alert-danger">
				{!!$errors->first('email') !!}
			</h6>
		@endif

		<p class="login-box-msg">Sign in to start your session</p>
			@if(Session::has('relogin'))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4><i class="icon fa fa-ban"></i> Alert!</h4>
					{!! Session::get('relogin') !!}
				</div>
			@endif

			{!! Form::open(['url' => 'c-panel/login', 'method' => 'post']) !!}
			<div class="form-group has-feedback">

				{!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				{!!$errors->first('email') !!}
			</div>
			<div class="form-group has-feedback">
				{!! Form::password('password', ['class' => 'form-control','placeholder'=>'Your Password','required']) !!}
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<input type="checkbox" name="remember" value="1"> Remember Me
						</label>
					</div>
				</div>
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
				</div>
				<!-- /.col -->
			</div>
		{!! Form::close() !!}




	</div>
	<!-- /.login-box-body -->
</div>
@endsection

