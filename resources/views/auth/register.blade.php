@extends('auth.layout')
@section('contents')
<div id="myModal" class="modal   fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Terms and Conditions</h4>
			</div>
			<div class="modal-body">
				{!! $terms->contents !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="register-box">
	<div class="register-logo">
		<a href="{{route('home')}}"><b>{{$profile->name}}</b> | Register</a>
	</div>

	<div class="register-box-body">
		<p class="login-box-msg">Register New Teacher</p>

		{!! Form::open(['url' => ['register'], 'method' => 'POST','class'=>'form-horizontal']) !!}
		<div class="box-body">
			@include('auth.register_form')
			<div class="form-group has-feedback {{$errors->has('password')?'has-error':''}}">
				{!! Form::label('password', 'Password', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::password('password', ['class' => 'form-control']) !!}
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					{!!$errors->first('password') !!}
				</div>
			</div>

			<div class="form-group has-feedback {{$errors->has('password_confirmation')?'has-error':''}}">
				{!! Form::label('password_confirmation', 'Password Confirmation', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					{!!$errors->first('password_confirmation') !!}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck ">
						<label>
							<label>
								{!! Form::checkbox('terms', '1', null,  ['id' => 'terms']) !!}
								I agree to the <a data-toggle="modal" data-target="#myModal">terms</a>
							</label>
							<span style="color: #dd4b39;; font-weight: 900">{!!$errors->first('terms') !!}</span>
						</label>
					</div>
				</div>
			{!! Form::hidden('registration', 1, ['id' => 'id']) !!}
			<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
				</div>
				<!-- /.col -->
			</div>
		</div>
		</form>

	</div>
	<!-- /.box-body -->

</div>
</div>
</div>


<a href="{{url('c-panel/login')}}" class="text-center">I already have a membership</a>
</div>
<!-- /.form-box -->
</div>
@endsection
