<div class="form-group has-feedback {{$errors->has('name')?'has-error':''}} ">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        {!!$errors->first('name') !!}
    </div>
</div>

<div class="form-group has-feedback {{$errors->has('email')?'has-error':''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        {!!$errors->first('email') !!}
    </div>
</div>

<div class="form-group has-feedback {{$errors->has('school')?'has-error':''}}">
    {!! Form::label('school', 'School', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('school', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon glyphicon-book form-control-feedback"></span>
        {!!$errors->first('school') !!}
    </div>
</div>

<div class="form-group has-feedback {{$errors->has('class_from')?'has-error':''}}">
    {!! Form::label('class_from', 'Class From', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::number('class_from', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon-book form-control-feedback"></span>
        {!!$errors->first('class_from') !!}
    </div>
</div>

<div class="form-group has-feedback {{$errors->has('class_to')?'has-error':''}}">
    {!! Form::label('class_to', 'Class To', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::number('class_to', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon-book form-control-feedback"></span>
        {!!$errors->first('class_to') !!}
    </div>
</div>

<div class="form-group has-feedback {{$errors->has('phone')?'has-error':''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::number('phone', null, ['class' => 'form-control']) !!}
        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
        {!!$errors->first('phone') !!}
    </div>
</div>
<div class="form-group has-feedback {{$errors->has('school_phone')?'has-error':''}}">
    {!! Form::label('school_phone', 'School Phone', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('school_phone', null, ['class' => 'form-control']) !!}
        <span class="glyphicon  glyphicon-phone-alt form-control-feedback"></span>
        {!!$errors->first('school_phone') !!}
    </div>
</div>


