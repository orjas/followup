@extends('backend.layout')

@section('mainContents')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add User
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box box-info">

                            <!-- /.box-header -->
                            <!-- form start -->

                                {!! Form::model($data, ['url' => $url, 'method' => 'post','class'=>'form-horizontal']) !!}
                                <div class="box-body">
                                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                        {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::text('name', null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('name') !!}
                                        </div>
                                    </div>


                                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                        {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::email('email', null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('email') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('password')?'has-error':''}}">
                                        {!! Form::label('Password', 'Password', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::password('password', ['class' => 'form-control'])!!}
                                            {!!$errors->first('password') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('confirm')?'has-error':''}}">
                                        {!! Form::label('Confirm Password',null, ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::password('password_confirmation', ['class' => 'form-control'])!!}
                                            {!!$errors->first('confirm') !!}

                                        </div>
                                    </div>


                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer text-center">

                                    <button type="submit" class="btn btn-info ">Submit</button>
                                </div>
                                <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>

            </div>


        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')
@endsection
