@extends('backend.layout')
@section('mainContents')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>User Form</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Users</a></li>
                <li class="active">New User Form</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Organizaton </th>
                                    <th>Remarks</th>
                                    <th>Person</th>
                                    <th>Status</th>
                                    <th>Next Followup</th>
                                    <th>User</th>
                                    <th>View Full Report</th>
                                    <th>Delete</th>


                                </tr>
                                </thead>

                                <tbody>

                                @foreach($datas as $index=>$data)

                                    <?php

                                    $people=\App\job::findOrfail($data->id)->people;
                                    $follows=\App\job::findOrfail($data->id)->followup;
                                            $user=\App\User::where('id',$data->user_id)->first();


                                    ?>
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->remarks}}</td>
                                        <td>
                                            <ol type="1">
                                                @foreach($people as $person)

                                                    <li>{{$person->p_name}}</li>

                                                @endforeach
                                            </ol>
                                        </td>
                                        <td>

                                            <ol type="1">
                                                @foreach($follows as $follow)




                                                    @if ($follow->f_status==1)
                                                        <li> Next Followup</li>
                                                    @elseif ($follow->f_status==2)
                                                        <li> Success</li>
                                                    @else
                                                        <li>Fail</li>
                                                    @endif



                                                @endforeach
                                            </ol>
                                        </td>

                                        <td>
                                            <ol type="1">
                                                @foreach($follows as $follow)


                                                    @if ($follow->followup_date==0)
                                                        <li> -</li>
                                                    @else
                                                        <li> {{$follow->followup_date}}</li>
                                                    @endif


                                                @endforeach
                                            </ol>
                                        </td>
                                        <td>{{$user->name}}</td>

                                        <td><a href="{{url('admin/job/'.$data->id.'/edit')}}">View Full Report</a></td>
                                        <td>

                                            {!! Form::open(['url' => 'admin/job/'.$data->id.'/delete', 'method' => 'post']) !!}
                                            <button type="button" class="btn btn-danger btn-sm btn-icon icon-left" href="#"
                                                    data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                                Delete</button>
                                            {!! Form::close() !!}
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    @include('backend.includes.showScripts')
@endsection
