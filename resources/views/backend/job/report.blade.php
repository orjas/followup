@extends('backend.layout')

@section('mainContents')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add User
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">User</a></li>
                <li class="active">form</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- SELECT2 EXAMPLE -->
            <div class="box box-default">

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">

                                <!-- /.box-header -->
                                <!-- form start -->


                                <div class="box-body">
                                    Organization Details


                                    <div class="form-group col-sm-12" >
                                        {!! Form::label('Name ', 'Name', ['class' => 'col-sm-4 control-label']) !!}
                                        {!! Form::label($job->name, $job->name, ['class' => 'col-sm-5 control-label']) !!}
                                        @if($job->user_id==Auth::user()->id)
                                          <div class="col-sm-3">
                                              {{--<a href="{{url('job/addFollowup')}}"><button  class="btn btn-info ">+</button></a>--}}
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                 Add Followup
                                              </button>

                                          </div>
                                            @endif

                                    </div>
                                <div class="form-group col-sm-12" >
                                    {!! Form::label('Address ', 'Address', ['class' => 'col-sm-4 control-label']) !!}
                                    {!! Form::label($job->address, $job->address, ['class' => 'col-sm-8 control-label']) !!}


                            </div>
                            <div class="form-group col-sm-12" >
                                {!! Form::label('Email ', 'Email', ['class' => 'col-sm-4 control-label']) !!}
                                {!! Form::label($job->email, $job->email, ['class' => 'col-sm-8 control-label']) !!}


                        </div>
                        <div class="form-group col-sm-12" >
                            {!! Form::label('Landline ', 'Landline', ['class' => 'col-sm-4 control-label']) !!}
                            {!! Form::label($job->landline, $job->landline, ['class' => 'col-sm-8 control-label']) !!}

                    </div>

                                    <div class="form-group col-sm-12" >
                                        {!! Form::label('Remarks ', 'Remarks', ['class' => 'col-sm-4 control-label']) !!}
                                        {!! Form::label($job->remarks, $job->remarks, ['class' => 'col-sm-8 control-label']) !!}

                                    </div>

                             {{--//model start--}}
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add Follow Up</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body " style="height: 500px">
                                                    <div class="nav-tabs-custom">
                                                        <ul class="nav nav-tabs">
                                                            {{--<li class="active"><a href="#tab_1" data-toggle="tab">Organization</a></li>--}}
                                                            <li class="active"><a href="#tab_2" data-toggle="tab">Representative</a></li>
                                                            <li><a href="#tab_3" data-toggle="tab">Follow Up</a></li>


                                                        </ul>
                                                        <div class="tab-content">


                                                            <div class="tab-pane active" id="tab_2">
                                                                <b>Person's Detail</b>
                                                                {!! Form::open( ['url' => 'job/'.$job->id.'/addFollowup', 'method' => 'post','class'=>'form-horizontal']) !!}
                                                                <div class="box-body">


                                                                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                                                        {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::text('p_name', null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('name') !!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group {{$errors->has('post')?'has-error':''}}">
                                                                        {!! Form::label('Post ', 'Post', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::text('p_post', null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('post') !!}
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                                                        {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::email('p_email', null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('email') !!}
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group {{$errors->has('mobile')?'has-error':''}}">
                                                                        {!! Form::label('Mobile', 'Mobile', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::text('p_mobile',null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('p_mobile') !!}

                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                                                        {!! Form::label('Remarks','Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::textarea('p_remarks',null, ['class' => 'form-control ','rows'=>2])!!}
                                                                            {!!$errors->first('confirm') !!}


                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <!-- /.box-body -->

                                                            </div>
                                                            <!-- /.tab-pane -->
                                                            <div class="tab-pane" id="tab_3">
                                                                <b>Follow Up </b>

                                                                <div class="box-body">
                                                                    <div class="form-group {{$errors->has('pitch')?'has-error':''}}">
                                                                        {!! Form::label('Pitch ', 'Pitch', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::text('f_pitch', null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('f_pitch') !!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group {{$errors->has('reponse')?'has-error':''}}">
                                                                        {!! Form::label('Response ', 'Response', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::textarea('f_response', null, ['class' => 'form-control' ,'rows'=>2])!!}
                                                                            {!!$errors->first('reposne') !!}
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                                                        {!! Form::label('f_Remarks ', 'Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::textarea('f_remarks', null, ['class' => 'form-control ', 'rows'=>2])!!}
                                                                            {!!$errors->first('remarks') !!}
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group {{$errors->has('status')?'has-error':''}}">
                                                                        {!! Form::label('Status', 'Status', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            <select  name='f_status' class="form-control" id="follow">
                                                                                <option value="1">Next Followup</option>
                                                                                <option value="2">Success</option>
                                                                                <option value="3">Fail</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group {{$errors->has('followup_date')?'has-error':''}}" id="date">
                                                                        {!! Form::label('Next Followup', 'Next Followup', ['class' => 'col-sm-1 control-label']) !!}
                                                                        <div class="col-sm-12">
                                                                            {!! Form::date('followup_date',null, ['class' => 'form-control'])!!}
                                                                            {!!$errors->first('followup_date') !!}

                                                                        </div>
                                                                    </div>



                                                                </div>
                                                                <!-- /.box-body -->
                                                                <div class="box-footer text-center">

                                                                    <button type="submit" class="btn btn-info ">Submit</button>
                                                                </div>
                                                                <!-- /.box-footer -->
                                                                {!! Form::close() !!}
                                                            </div>
                                                            <!-- /.tab-pane -->
                                                        </div>
                                                        <!-- /.tab-content -->
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--//model end--}}

                                    @foreach($merges as $index=>$merge)
                                <?php

                                        $first=$index."1";
                                        $second=$index."2";
                                        ;?>
                                    <div class="col-md-6 " style="min-height: 350px">
                                        <!-- Horizontal Form -->
                                        <div class="box box-info">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"> Follow</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <!-- form start -->
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    {{--<li class="active"><a href="#tab_1" data-toggle="tab">Organization</a></li>--}}
                                                    <li class="active"><a href="#tab_{{$first}}" data-toggle="tab">Representative</a></li>
                                                    <li><a href="#tab_{{$second}}" data-toggle="tab">Follow Up</a></li>


                                                </ul>
                                                <div class="tab-content">


                                                    <div class="tab-pane active" id="tab_{{$first}}">

                                                        <div class="box-body">

                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Name ', 'Name', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10 " contenteditable="true">

                                                                     <p>   {!! $merge->p_name !!}</p>
                                                                    </div>

                                                                </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Post ', 'Post', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->p_post !!}
                                                                    </div>

                                                                </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Mobile ', 'Mobile', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->p_mobile !!}
                                                                    </div>

                                                                </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Email ', 'Email', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->p_email !!}
                                                                    </div>

                                                                </div>

                                                        </div>
                                                        <!-- /.box-body -->

                                                    </div>
                                                    <!-- /.tab-pane -->
                                                    <div class="tab-pane" id="tab_{{$second}}">


                                                        <div class="box-body">

                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Pitch ', 'Pitch', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->f_pitch !!}
                                                                    </div>

                                                                </div>
                                                            <div class="form-group col-sm-12" >
                                                                {!! Form::label('Remarks ', 'Remarks', ['class' => 'col-sm-2 control-label']) !!}
                                                                <div class="control-label col-sm-10">
                                                                    {!! $merge->p_remarks !!}
                                                                </div>

                                                            </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Reponse ', 'Response', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->f_response !!}
                                                                    </div>

                                                                </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Remarks ', 'Remarks', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                    {!! $merge->f_remarks !!}
                                                                    </div>

                                                                </div>
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Status ', 'Status', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        @if ($merge->f_status==1)
                                                                            Next Followup
                                                                        @elseif ($merge->f_status==2)
                                                                            Success
                                                                        @else
                                                                            Fail
                                                                        @endif
                                                                    </div>
                                                                    </div>
                                                                 @if($merge->f_status==1)
                                                                <div class="form-group col-sm-12" >
                                                                    {!! Form::label('Followup Date ', 'Remarks', ['class' => 'col-sm-2 control-label']) !!}
                                                                    <div class="control-label col-sm-10">
                                                                        {!! $merge->followup_date !!}
                                                                    </div>

                                                                </div>
                                                                     @endif

                                                        </div>
                                                        <!-- /.box-body -->

                                                    </div>
                                                    <!-- /.tab-pane -->
                                                </div>
                                                <!-- /.tab-content -->

                                            </div>
                                        </div>
                                    </div>
                                        {{--model for Edit--}}
                                        {{--end of model--}}

                                        @endforeach



                                </div>
                                <!-- /.box-body -->



                        </div>



                    </div>

                </div>


            </div>
                </div>
            <!-- /.box -->


        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')


    <script>
        $("#follow").click(function(){
            var e = this.value;
            if(e!=1){
                $("#date").hide();
            }
            else{
                $("#date").show();
            }

        })
    </script>
@endsection
