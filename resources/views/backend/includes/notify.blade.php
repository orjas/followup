<?php
$failed_message='';
$success_message='';
if(Session::has('failed_message'))
    $failed_message=Session::get('failed_message');

if(Session::has('success_message'))
    $success_message=Session::get('success_message');
;?>

<script src="{{asset('common/notify.min.js')}}"></script>
<script>
    var failed_message="{!! $failed_message !!}";
    var success_message="{!!  $success_message!!}";
    if(!(success_message==""&& success_message==null)){
        $.notify(success_message,'success',
            {position: "right"});
    }

    if(!(failed_message=""&&failed_message==null)){
        $.notify(failed_message,'error',
            {position: "right"});
    }
</script>