<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                    <img src="{{asset('buzz.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>




            {{--users--}}
            <li class="treeview users ">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Users</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="users-all">
                        <a href="{{url('admin/user/view')}}">
                            <i class="fa fa-calendar"></i> <span>VIEW ALL</span>
                            <span class="pull-right-container">
                                    <small class="label pull-right bg-red">{{\App\User::where('type',2)->count()}}</small>
                                    <small class="label pull-right bg-blue"></small>
                             </span>
                        </a>
                    </li>
                    <li class="users-add"><a href="{{url('admin/user/add')}}"><i class="fa fa-circle-o"></i>ADD</a></li>
                </ul>
            </li>






            <li>
                <a href="{{url('admin/job/view')}}">
                    <i class="fa fa-calendar"></i> <span>View Job</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">{{\App\job::count()}}</small>
              <small class="label pull-right bg-blue"></small>
            </span>
                </a>
            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>