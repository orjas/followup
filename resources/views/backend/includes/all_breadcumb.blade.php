<section class="content-header">
    <h1>{{$table['all']['title']}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url($table['action'])}}">{{$table['title']}}</a></li>
        <li class="active">{{$table['all']['title']}}</li>
    </ol>
</section>