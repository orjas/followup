<section class="content-header">
    <h1>{{$table['form']['title']}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$table['form']['title']}}</li>
    </ol>
</section>