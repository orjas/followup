<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('buzz.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{url('user/dashboard')}}">Dashboard</a></li>




            {{--users--}}
            <li class="treeview users ">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>JOB</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="users-all">
                        <a href="{{url('job/view')}}">
                            <i class="fa fa-calendar"></i> <span>VIEW ALL</span>
                            <span class="pull-right-container">
                                    <small class="label pull-right bg-red">{{\App\job::count()}}</small>
                                    <small class="label pull-right bg-blue"></small>
                             </span>
                        </a>
                    </li>
                    <li class="users-add"><a href="{{url('job/add')}}"><i class="fa fa-circle-o"></i>ADD NEW JOB</a></li>
                    <li class="questions-add"><a href="{{url('job/addFollowup')}}"><i class="fa fa-circle-o"></i>ADD FollowUp</a></li>

                </ul>
            </li>
            <li class="treeview users ">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Todo</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="users-all">
                        <a href="{{url('todo/view')}}">
                            <i class="fa fa-calendar"></i> <span>VIEW ALL</span>
                            <span class="pull-right-container">
                                    <small class="label pull-right bg-red">{{\App\Todo::where('user_id',\Illuminate\Support\Facades\Auth::user()->id)->count()}}</small>
                                    <small class="label pull-right bg-blue"></small>
                             </span>
                        </a>
                    </li>
                    <li class="users-add"><a href="{{url('todo/add')}}"><i class="fa fa-circle-o"></i>ADD NEW Todo</a></li>

                </ul>
            </li>











        </ul>
    </section>
    <!-- /.sidebar -->
</aside>