<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Developed By</b>  <a href="http://www.slashplus.com" target="_blank">SLASH PLUS Pvt. Ltd.</a>
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a href="{{Config::get('constants.SITE_URL')}}" target="_blank">profilename</a>.</strong> All rights
    reserved.
</footer>