<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>S</b>ITE</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b> hello</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
<?php
   $thisdate=$now->format('Y-m-d');
        ?>

        <div class="navbar-custom-menu  " id="app" >
            <div class="nav navbar-nav">

                {{--MessageNotify Start--}}
                @if(\Illuminate\Support\Facades\Auth::user()->type==2)
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success" v-cloak >

                       @{{ todo_count }}</span>


                    </a>
                    <ul class="dropdown-menu col-md-12"  style="width: auto">
                        <li class="col-md-12"  >
                            <ul class="menu col-md-12 " v-if="todo_count > 0">


                                <li class="" v-for="ad in todo_results " >
                                <a href="{{url('todo/view')}}" >


                                <i class="fa fa-users text-aqua"></i> @{{ad.message}}



                                </a>
                                </li>

                            </ul>
                        </li>
                        <li class="footer col-md-12 " v-if="todo_count > 0"><a href="{{url('message/view')}}">View all</a></li>

                    </ul>
                </li>
                @endif
                {{--MessageNotify End--}}



                {{--NotificationStart--}}

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning" >

                            @{{job_count}}
                        </span>
                    </a>
                    <ul class="dropdown-menu"  style="width: auto">
<li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu" v-if="job_count > 0">
                            <li class="" v-for="job in jobhtml" v-html="job" >


                       </li>


                        </ul>
</li>


                      @if(\Illuminate\Support\Facades\Auth::user()->type==1)
                        <li class="footer " v-if="job_count > 0"><a href="{{url('admin/notification/view')}}">View all</a></li>
                      @elseif(\Illuminate\Support\Facades\Auth::user()->type==2)
                            <li class="footer " v-if="job_count > 0"><a href="{{url('notification/view')}}">View all</a></li>

                        @endif
                    </ul>
                </li>

                    {{--Notification End--}}
                            <!-- User Account: style can be found in dropdown.less -->

                    {{--NotificationEnd--}}







                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('buzz.png')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('buzz.png')}}" class="img-circle" alt="User Image">

                            <p>
                                hello
                                <small>{{\Illuminate\Support\Facades\Auth::user()->name}}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">

                                <div class="pull-left">
                                    <a href="{{url('profile')}}" class="btn btn-default btn-flat">PROFILE</a>
                                </div>

                            <div class="pull-right">
                                <a href="{{url('c-panel/logout')}}" class="btn btn-default btn-flat">LOGOUT</a>
                            </div>
                        </li>
                    </ul>
                </li>
                </ul>
            </div>
            </div>
    </nav>
</header>