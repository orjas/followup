

<!-- jQuery 2.2.3 -->
<script src="{{asset('backend/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('backend/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset("backend/notify.js")}}"></script>
<!-- InputMask -->
<script src="{{asset('backend/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('backend/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('backend/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{asset('backend/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/app.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<!-- Page script -->
<script>
    $(function () {

        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format:'yyyy-mm-dd',

        });
          $(function () {
            $("#datetimepicker2,#datetimepicker1").datetimepicker({
            });
        });



        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });


});
</script>




<script type="text/javascript" src="{{ asset('js/vue.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/tinymce/tinymce.min.js') }}"></script>
<?php
$siteUrl=\Illuminate\Support\Facades\Config::get('constants.SITE_URL');
?>
<script type="text/javascript">


    tinymce.init({

        forced_root_block : '',
        selector: ".editor",theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code",
            'fullscreen'
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code |fontselect fontsizeselect |fontselect fontname ",

        image_advtab: true ,
        relative_urls:false,

        external_filemanager_path:"{{$siteUrl}}/backend/plugins/tinymce/plugins/filemanager/",
        filemanager_title:"Filemanager" ,
        external_plugins: { "filemanager" : "{{$siteUrl}}/backend/plugins/tinymce/plugins/filemanager/plugin.min.js"}
    });
       tinymce.init({
        selector: '.simpleEditor',  // change this value according to your HTML
        menu: {
            view: {title: 'Edit', items: 'cut, copy, paste'}
        }
    });
</script>
