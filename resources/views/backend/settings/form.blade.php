@extends($extend)

@section('mainContents')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @include('backend.includes.single_breadcumb')

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">


                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <?php
                            $c='';
                            if(\Illuminate\Support\Facades\Auth::user()->type==2){
                                $c='disabled';


                            }
                            ?>

                            {!! Form::open( ['url' => 'profile', 'method' => 'post','class'=>'form-horizontal']) !!}

                                <div class="box-body">

                                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                        {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::text('name', Auth::user()->name, ['class' => 'form-control', $c])!!}
                                            {!!$errors->first('name') !!}
                                        </div>
                                    </div>


                                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                        {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::email('email', Auth::user()->email, ['class' => 'form-control',$c])!!}
                                            {!!$errors->first('email') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('password')?'has-error':''}}">
                                        {!! Form::label('Password', 'Password', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::password('password', ['class' => 'form-control'])!!}
                                            {!!$errors->first('password') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('confirm')?'has-error':''}}">
                                        {!! Form::label('Confirm Password',null, ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::password('password_confirmation', ['class' => 'form-control'])!!}
                                            {!!$errors->first('confirm') !!}

                                        </div>
                                    </div>
</div>
                                <!-- /.box-body -->

                                <div class="box-footer ">

                                    {{--<button type="submit" class="btn btn-danger">Cancel</button>--}}
                                    <button type="submit" class="btn btn-info pull-right">Update</button>
                                </div>
                                <!-- /.box-footer -->

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>

            </div>


        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')
    <script>
        $(".sidebar-menu").find(".profile").addClass('active');
    </script>
@endsection
