
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FollowUp </title>
   @include('backend.includes.styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
@include('backend.includes.delete')
<div class="wrapper">

@include('backend.includes.header')
<?php $message='';?>
<?php $message= Session::get('alert-success');?>
<!-- Left side column. contains the logo and sidebar -->
    @include('backend.includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
    @if(\Illuminate\Support\Facades\Session::has('message'))
        <p class="alert {{ \Illuminate\Support\Facades\Session::get('alert-class', 'alert-info') }}">{{ \Illuminate\Support\Facades\Session::get('message') }}</p>
        @endif
    @yield('mainContents')

    <!-- /.content-wrapper -->
@include('backend.includes.footer')


<!-- Control Sidebar -->
@include('backend.includes.headerSideBar')

<!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@yield('scripts')


<script>

    var success="{{$message}}";

    if(success!=='') {
        $.notify(
                success,
                { className:'success',
                    globalPosition: 'top center'}
        );
    }

    var app = new Vue({
        el: '#app',
        data: {

            todo_results:null,
            job_results:null,
            todo_count:null,
            job_count:null,
            interval:null,
            jobhtml:[]
        },



        created:function(){
            this.loadData();

            this.interval = setInterval(function () {

                this.loadData();



            }.bind(this), 5000);
        },
        methods: {
            loadData: function () {
                var self=this;
                axios.get("{{url('notify-header-data')}}", {


                })

                        .then(function (response) {


                            self.job_results=response.data.job_notification;
                            self.job_count=response.data.job_notification.length;
                            self.todo_results=response.data.todos;
                            self.todo_count=response.data.todos.length;
                            if(self.job_results.length>0){
                                $( self.job_results ).each(function( index,value ) {
                                    self.jobhtml[index]='<a href="{{url("job")}}/'+value.job_id+'/edit"  style="color: #3c8dbc">'+'<i class="fa fa-users text-aqua"></i>'+value.messageAdmin+'</a>';






                                })
                            }

                        })
                        .catch(function (error) {
                            console.log(error);
                        }.bind(this));
            }
        },

        beforeDestroy: function(){

            clearInterval(this.interval);

        }
    })
</script>

</body>
</html>
