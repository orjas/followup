
@extends($extend)


@section('mainContents')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Notification
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">User</a></li>
                <li class="active">form</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10">
                    <!-- The time line -->
                    <ul class="timeline">

                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        @if(\Illuminate\Support\Facades\Auth::user()->type==1)
                            @foreach($admin as $ad)
                                <?php
                                $job=\App\job::findOrfail($ad->job_id);
                                $u=\App\User::findOrfail($ad->user_id);
                                ?>
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>

                            <div class="timeline-item">




                                <div class="timeline-body">
                                    @if($ad->date==$thisdate)

                                         Tommorow,{{$ad->remind_me}},{{$u->name}} have a followup for the job:
                                    @elseif($ad->followup_date==$thisdate && $ad->remind_me!=($now->format('Y-m-d H:i')))
                                        Today,{{$ad->remind_me}},{{$u->name}} have a followup  for the job:
                                    @endif
                                        {{$job->name.'-'.$job->remarks}}

                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs"  href="{{url('job/'.$ad->job_id.'/edit')}}">View Detail</a>

                                </div>
                            </div>
                        </li>
                            @endforeach
                        @endif
                        <!-- END timeline item -->
                        <!-- timeline item -->
                                    @if(\Illuminate\Support\Facades\Auth::user()->type==2)
                                        @foreach($user as $ad)
                                            <?php
                                            $job=\App\job::findOrfail($ad->job_id);
                                            $u=\App\User::findOrfail($ad->user_id);
                                            ?>
                                            <li>
                                                <i class="fa fa-envelope bg-blue"></i>

                                                <div class="timeline-item">




                                                    <div class="timeline-body">
                                                        @if($ad->date==$thisdate)

                                                            Tommorow,{{$ad->remind_me}},You have a followup for the job:
                                                        @elseif($ad->followup_date==$thisdate && ($ad->remind_me)>=($now->format('Y-m-d H:i')))
                                                            Today,{{$ad->remind_me}},You have a followup for the job:
                                                        @endif
                                                        {{$job->name.'-'.$job->remarks}}

                                                    </div>
                                                    <div class="timeline-footer">
                                                        <a class="btn btn-primary btn-xs"  href="{{url('job/'.$ad->job_id.'/edit')}}">View Detail</a>

                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                    </ul>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')

@endsection