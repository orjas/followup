
@extends('backend.layoutFront')


@section('mainContents')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Notification
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">User</a></li>
                <li class="active">form</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10">
                    <!-- The time line -->
                    <ul class="timeline">

                        <!-- /.timeline-label -->
                        <!-- timeline item -->

                            @foreach($todos as $do)
                                <?php
                                $job=\App\Todo::findOrfail($do->todo_id);

                                ?>
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>

                            <div class="timeline-item">




                                <div class="timeline-body">
                                    You have a todo list at,{{ date('h:i', strtotime($do->followup_date))}},   for the job:-<br>


                                    {!! $job->name.'-'.$job->remarks !!}

                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs"  href="{{url('todo/view')}}">View Detail</a>

                                </div>
                            </div>
                        </li>
                            @endforeach

                        <!-- END timeline item -->
                        <!-- timeline item -->

                    </ul>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')

@endsection