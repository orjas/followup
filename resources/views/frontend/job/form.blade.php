@extends('backend.layoutFront')

@section('mainContents')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add User
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->

        <!-- /.box -->
        <div class="row">
            <div class="col-md-10">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Organization</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Representative</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Follow Up</a></li>

                        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <b>Organization Detail</b>

                            {!! Form::open( ['url' => 'job/add', 'method' => 'post','class'=>'form-horizontal']) !!}
                            <div class="box-body">
                                <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                    {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('name', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('name') !!}
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('address')?'has-error':''}}">
                                    {!! Form::label('Address ', 'Address', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('address', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('address') !!}
                                    </div>
                                </div>


                                <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                    {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::email('email', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('email') !!}
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('landline')?'has-error':''}}">
                                    {!! Form::label('Landline', 'Landline', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('landline',null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('landline') !!}

                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                    {!! Form::label('Remarks','Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('remarks',null, ['class' => 'form-control '])!!}
                                        {!!$errors->first('confirm') !!}

                                    </div>
                                </div>


                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <b>Person's Detail</b>

                            <div class="box-body">
                                <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                    {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('p_name', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('name') !!}
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('post')?'has-error':''}}">
                                    {!! Form::label('Post ', 'Post', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('p_post', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('post') !!}
                                    </div>
                                </div>


                                <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                    {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::email('p_email', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('email') !!}
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('mobile')?'has-error':''}}">
                                    {!! Form::label('Mobile', 'Mobile', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('p_mobile',null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('p_mobile') !!}

                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                    {!! Form::label('Remarks','Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::textarea('p_remarks',null, ['class' => 'form-control '])!!}
                                        {!!$errors->first('confirm') !!}

                                    </div>
                                </div>


                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <b>Follow Up </b>

                            <div class="box-body">
                                <div class="form-group {{$errors->has('pitch')?'has-error':''}}">
                                    {!! Form::label('Pitch ', 'Pitch', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::text('f_pitch', null, ['class' => 'form-control'])!!}
                                        {!!$errors->first('pitch') !!}
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('reponse')?'has-error':''}}">
                                    {!! Form::label('Response ', 'Response', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::textarea('f_response', null, ['class' => 'form-control '])!!}
                                        {!!$errors->first('reposne') !!}
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                    {!! Form::label('f_Remarks ', 'Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        {!! Form::textarea('f_remarks', null, ['class' => 'form-control '])!!}
                                        {!!$errors->first('remarks') !!}
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('status')?'has-error':''}}">
                                    {!! Form::label('Status', 'Status', ['class' => 'col-sm-1 control-label']) !!}
                                    <div class="col-sm-12">
                                        <select  name='f_status' class="form-control" id="follow">
                                            <option value="1">Next Followup</option>
                                            <option value="2">Success</option>
                                            <option value="3">Fail</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group {{$errors->has('followup_date')?'has-error':''}} " id="date">
                                    {!! Form::label('Next Followup', 'Next Followup', ['class' => 'col-sm-12 control-label']) !!}

                                    <div class="col-sm-6" >
                                        <div class='input-group date col-sm-6' id='datetimepicker1'>
                                            <input type='text' class="form-control" name="followup_date"/>
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                                           </span>
                                        </div>
                                    </div>






                                </div>



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">

                                <button type="submit" class="btn btn-info ">Submit</button>
                            </div>
                            <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>

    </section>
    <!-- /.content -->
</div>

@endsection

@section('scripts')

    @include('backend.includes.formScripts')
    <script>
        $("#follow").click(function(){
        var e = this.value;
            if(e!=1){
                $("#date").hide();
            }
            else{
            $("#date").show();
            }

        });

    </script>
@endsection
