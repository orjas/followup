@extends('backend.layoutFront')
@section('mainContents')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>User Form</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Users</a></li>
                <li class="active">New User Form</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Landline</th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                    <th>Followup Date</th>
                                    <th>Edit</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($todos as $index=>$todo)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$todo->name}}</td>
                                        <td>{{$todo->address}}</td>
                                        <td>{{$todo->email}}</td>
                                        <td>{{$todo->landline}}</td>

                                        <td>{!! $todo->remarks !!}</td>
                                        <td>
                                            <ul>
                                            @if ($todo->status==1)
                                                <li> Next Followup</li>
                                            @elseif ($todo->status==2)
                                                <li> Success</li>
                                            @else
                                                <li>Fail</li>
                                            @endif
                                            </ul>
                                        </td>
                                        <td>
                                            @if ($todo->followup_date==0)
                                                <li> -</li>
                                            @else
                                                <li> {{$todo->followup_date}}</li>
                                            @endif
                                        </td>

                                        <td><a href="{{url('todo/'.$todo->id.'/edit')}}">Edit</a></td>
                                        <td>
                                            {!! Form::open(['url' => 'todo/'.$todo->id. '/delete','method'=>'post']) !!}
                                            <button type="button" class="btn btn-danger btn-sm btn-icon icon-left" href="#"
                                                    data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                                Delete</button>
                                            {!! Form::close() !!}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    @include('backend.includes.showScripts')
@endsection
