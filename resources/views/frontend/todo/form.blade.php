@extends('backend.layoutFront')

@section('mainContents')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            To do
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box box-info">

                            <!-- /.box-header -->
                            <!-- form start -->

                                {!! Form::model($data, ['url' => $url, 'method' => 'post','class'=>'form-horizontal']) !!}
                                <div class="box-body">
                                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                                        {!! Form::label('Name ', 'Name', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::text('name', null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('name') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('address')?'has-error':''}}">
                                        {!! Form::label('Address ', 'Address', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::text('address', null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('address') !!}
                                        </div>
                                    </div>


                                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                                        {!! Form::label('Email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::email('email', null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('email') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('landline')?'has-error':''}}">
                                        {!! Form::label('Landline', 'Landline', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::text('landline',null, ['class' => 'form-control'])!!}
                                            {!!$errors->first('landline') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('remarks')?'has-error':''}}">
                                        {!! Form::label('Remarks','Remarks', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::textarea('remarks',null, ['class' => 'form-control '])!!}
                                            {!!$errors->first('confirm') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('status')?'has-error':''}}">
                                        {!! Form::label('Status', 'Status', ['class' => 'col-sm-1 control-label']) !!}
                                        <div class="col-sm-12">

                                            {!! Form::select('status', $categories,$select, ['class'=>'form-control','id'=>'follow']) !!}
                                        </div>
                                    </div>

                                    <div id="date">

                                    <div class="form-group {{$errors->has('followup_date')?'has-error':''}} " >
                                        {!! Form::label('Next Followup', 'Next Followup', ['class' => 'col-sm-2 control-label']) !!}
                                        {!! Form::label('Remind Me', 'Remind me', ['class' => 'col-sm-6 control-label']) !!}

                                        <div class="col-sm-6" >
                                            <div class='input-group date col-sm-12' id='datetimepicker1'>
                                                {!! Form::text('followup_date',null, ['class' => 'form-control ','rows'=>1])!!}


                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                                           </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" >
                                            <div class='input-group date col-sm-10' id='datetimepicker2'>
                                                <input type='text' class="form-control" name="remind_me" />
                                            <span class="input-group-addon">
                                               <span class="glyphicon glyphicon-calendar"></span>
                                                           </span>
                                            </div>
                                        </div>





                                    </div>
                                    </div>


                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer text-center">

                                    <button type="submit" class="btn btn-info ">Submit</button>
                                </div>
                                <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>

            </div>


        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
@endsection

@section('scripts')
    @include('backend.includes.formScripts')
    <script>
        var e='{{$select}}';

        if(e!=1){
            $("#date").hide();
        }
        else{
            $("#date").show();
        }
        $("#follow").click(function(){
            var e = this.value;

            if(e!=1){
                $("#date").hide();
            }
            else{
                $("#date").show();
            }

        })
    </script>
@endsection
