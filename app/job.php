<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    public function people()
    {
        return $this->hasMany('App\person')->orderBy('fu_conter','desc');
    }
    public function followup(){
        return $this->hasMany('App\Follow')->orderBy('fu_conter','desc');
    }

    protected $fillable = ['name', 'email', 'landline','address','remarks','user_id'];
}
