<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class person extends Model
{
    //
    protected $fillable = ['p_name', 'p_post', 'p_counter','p_mobile','p_email','job_id'];
}
