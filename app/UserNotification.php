<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    //
    protected $fillable = ['followup_date','user_status','user_id','todo_id','remind_me','message'];

}
