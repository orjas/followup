<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['name', 'email', 'landline','address','remarks','user_id','status','followup_date'];

}
