<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = ['p_remarks', 'p_pitch', 'p_response','f_counter','f_remarks','f_status','followup_date','job_id'];
    //
}
