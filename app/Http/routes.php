<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('c-panel/login');
});
Route::get('admin/dashboard',function(){
    return view('backend.dashboard.dashboard');
});

//    function(){
//
//});

Route::get('c-panel/login','Auth\AuthController@getLogin')->name('getLogin');
Route::post('c-panel/login','Auth\AuthController@postLogin')->name('postLogin');
Route::get('c-panel/logout','Auth\AuthController@getLogout')->name('logout');
//user
Route::group(['middleware'=>'auth'],function(){

//Admin
Route::group(['middleware'=>'admin'],function(){
//job
    Route::get('job/add',function(){
        return view('frontend.job.form');
    });
    Route::post('job/add','JobController@store');
    Route::get('admin/job/view','JobController@show');
    Route::post('admin/job/{id}/delete','JobController@destroy');
    Route::get('admin/job/{id}/edit','JobController@edit');
    Route::post('admin/job/{id}/update','JobController@update');
    //profile
    Route::get('admin/dashboard',function(){
       return view('backend.dashboard.dashboard');
    });

    //user

    Route::get('/admin/user/add','Admin\UserController@index');

    Route::post('/admin/user/add','Admin\UserController@store');
    Route::get('/admin/user/view','Admin\UserController@show');
    Route::post('/admin/user/{id}/delete','Admin\UserController@destroy');
    Route::get('/admin/user/{id}/edit','Admin\UserController@edit');
    Route::post('/admin/user/{id}/update','Admin\UserController@update');
    //
    Route::get('admin/notification/view','NotificationController@index');
});
//organization
Route::group(['middleware'=>'user'],function(){
    Route::get('user/dashboard',function(){
        return view('frontend.dashboard.dashboard');
    });
    //Notification
    Route::get('job/add',function(){
        return view('frontend.job.form');
    });
    Route::post('job/add','JobController@store');
    Route::get('job/view','JobController@show');
    Route::post('job/{id}/delete','JobController@destroy');
    Route::get('job/{id}/edit','JobController@edit');
    Route::post('job/{id}/update','JobController@update');

    //Follouup
    Route::get('job/addFollowup','FollowupController@index');
    Route::get('job/searchjob','FollowupController@search');
    Route::post('job/{id}/addFollowup','FollowupController@store');
    Route::get('user/job/{id}/edit','FollowupController@edit');
    Route::get('user/follow/{id}/edit','FollowupController@editFollow');
    Route::post('user/job/{id}/update','FollowupController@update');
    Route::post('user/follow/{id}/update','FollowupController@updateFollow');

    //Notification
    Route::get('notification/view','NotificationController@index');
//todouser

    Route::get('todo/add','TodoController@index');
    Route::post('todo/add','TodoController@store');
    Route::get('todo/view','TodoController@show');
    Route::post('todo/{id}/delete','TodoController@destroy');
    Route::get('todo/{id}/edit','TodoController@edit');
    Route::post('todo/{id}/update','TodoController@update');



    Route::get('message/view','TodoController@notify');




});
Route::get('job/{id}/edit','JobController@edit');
Route::get('notify-header-data','TodoController@header_data');
Route::get('profile','Admin\ProfileController@index');
Route::post('profile','Admin\ProfileController@store');
Route::get('get-person-data','JobController@getPerson');

});