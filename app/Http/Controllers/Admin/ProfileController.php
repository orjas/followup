<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function index()
    {
        $extend='backend.layout';
        if(Auth::user()->type==2)
            $extend='backend.layoutFront';
        return view('backend.settings.form',compact('extend'));
}
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'min:4|max:255',
            'email' => 'email',
            'password'=>'confirmed|min:8'

        ]);

        $id = Auth::user()->id;
        $check = 0;

        $user = User::findOrFail($id);
        if($request->has('name'))
             $user->name = $request->name;
        if ($request->has('email') && $request->email != $user->email) {
           $this->validate($request,[
              'email'=>'unique:users'
           ]);
            $user->email = $request->email;
            $check = 1;
        }
        if ($request->has('password')) {
                 $user->password = bcrypt($request->password);
                $check = 1;
            }



        $user->update();
        if($check==0){
            $request->session()->flash('alert-success', 'Successful Updated!');


        }
        else{
            $request->session()->flash('alert-success', 'Successfully updated!Next Time  login with new Credential');


        }
        return redirect('profile');


    }


}
