<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $text='Add User';
        $data='';
        $url='admin/user/add';

        return view('backend.user.form',compact('text','data','url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);



        $data=$request->all();
        $data['type']=2;
        $data['password']=bcrypt($request->password);
        $user=User::create($data);
        $request->session()->flash('alert-success', 'Successful ADDED');
        return redirect('admin/user/view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $datas=User::where('type',2)->get();
        return view('backend.user.show',compact('datas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=User::findOrfail($id);
        $text='Edit  User';

        $url='/admin/user/'.$id.'/update';
        return view('backend.user.form',compact('data','url','text'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|min:6',
        ]);



        $data=$request->all();
        $data['type']=2;

        if($request->password) {
            $data['password'] = bcrypt($request->password);
        }
            $user=User::findOrfail($id);
        if($request->email!=$user->email){

            $this->validate($request,[
                'email'=>'unique:users'
            ]);
        }
        $user->update($data);
        $request->session()->flash('alert-success', 'Successful Updated');
        return redirect('admin/user/view');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $user=User::findOrfail($id);
        $user->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Successful Deleted');

        return redirect('/admin/user/view');
    }
}
