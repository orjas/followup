<?php

namespace App\Http\Controllers;

use App\Follow;
use App\job;
use App\Notification;
use App\person;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FollowupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs=job::where('user_id',Auth::user()->id)->get();
        $show=0;
        return view('frontend.job.follow',compact(['jobs','show']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        if($request->search==''){
            $request->session()->flash('alert-success', 'No Search to display');
            return redirect('job/view');
        }
        $url='job/'.$request->search.'/addFollowup';
        $jobs=job::all();
        $org=job::findOrfail($request->search);
        $show=1;
        return view('frontend.job.follow',compact(['jobs','show','url','org']));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,$id)
    {

        $c=1;
        $j=person::where('job_id',$id)->orderBy('fu_conter','desc')->first(['fu_conter']);


        if($j->fu_conter!=0){
            $c=++$j->fu_conter;
        }

        //
        $person=new person();
        $follow=new Follow();
        $person->p_name=$request->p_name;
        $person->p_post=$request->p_post;
        $person->p_email=$request->p_email;
        $person->p_mobile=$request->p_mobile;
        $person->fu_conter=$c;
        $person->job_id=$id;
        $person->save();
        $follow->f_pitch=$request->f_pitch;
        $follow->p_remarks=$request->p_remarks;
        $follow->f_response=$request->f_response;
        $follow->f_remarks=$request->f_remarks;
        $follow->f_status=$request->f_status;
        if($request->f_status==1){
            $notify=Notification::where('job_id',$id)->first();
            if($notify){
                $notify->delete();
            }
            $job=job::findOrfail($id);


            $follodate= date('Y-m-d H:i:s', strtotime("$request->followup_date"));
            $follow->followup_date=$follodate;
            $time=date('Y-m-d h:i', strtotime("$request->followup_date"));


            $data=[
                'message'=>'You have a FollowUp at '.$time.' for the job:<b> '.$job->name.'-'.$job->remarks.'</b>',
                'messageAdmin'=>Auth::user()->name.' have a FollowUp at '.$time.' for the job:<b> '.$job->name.'-'.$job->remarks.'</b>',
                'fu_conter'=>$c,
                'followup_date'=> date('Y-m-d 00:00:00', strtotime("$request->followup_date")),
                'remind_me'=>$follodate,
                'date'=>date('Y-m-d', strtotime('-1 day', strtotime($request->followup_date))),
                'admin_status'=>0,
                'user_status'=>0,
                'type'=>1,
                'user_id'=>Auth::user()->id,
                'job_id'=>$id
            ];

            Notification::create($data);
        }
        else{
            $notify=Notification::where('job_id',$id)->first();
            if($notify){
            $notify->delete();
            }
        }
        $follow->fu_conter=$c;
        $follow->job_id=$id;
        $follow->save();
        $request->session()->flash('alert-success', 'Successful ADDED Followup');

        return redirect('job/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobs=job::findOrfail($id);
        $show=1;
        return view('frontend.job.job',compact('show','jobs'));

    }
    public function editFollow($id)
    {

    $categories=[
      "1"=>"Next Followup",
        "2"=>"Success",
        "3"=>"Fail",
    ];
        $show=20;


$person=person::findOrfail($id);

        $job=$person->job_id;
      $jobs=$person->join('follows','follows.job_id','=','people.job_id')
          ->where('follows.job_id',$job)
          ->where('follows.fu_conter',$person->fu_conter)
          ->where('people.id',$id)
          ->first();


        return view('frontend.job.job',compact('jobs','categories','show'));







    }


    public function update(Request $request, $id)
    {

$jobs=job::findOrfail($id);
        $data=$request->all();
        $jobs->update($data);
        $request->session()->flash('alert-success', 'Successful Updated Job');

        return redirect('job/'.$id.'/edit');

    }
    public function updateFollow(Request $request, $id)
    {

        $follow=Follow::findOrfail($id);
        $jobid=$follow->job_id;
        $fuconter=$follow->fu_conter;
        $person=person::where('job_id',$jobid)->where('fu_conter',$fuconter)->first();
        $person->p_name=$request->p_name;
        $person->p_post=$request->p_post;
        $person->p_email=$request->p_email;
        $person->p_mobile=$request->p_mobile;
        $person->save();
        $follow->f_pitch=$request->f_pitch;
        $follow->p_remarks=$request->p_remarks;
        $follow->f_response=$request->f_response;
        $follow->f_remarks=$request->f_remarks;
        $follow->f_status=$request->f_status;

        if($request->f_status==1){
            $notify=Notification::where('job_id',$jobid)->first();
            if($notify){
                $notify->delete();
            }

            $job=job::findOrfail($jobid);


            $follodate= date('Y-m-d H:i:s', strtotime("$request->followup_date"));
            $follow->followup_date=$follodate;

            $time=date('Y-m-d h:i', strtotime("$request->followup_date"));

            $data=[
                'messageAdmin'=>Auth::user()->name.' have a FollowUp at '.$time.' for the job:<b> '.$job->name.'-'.$job->remarks.'</b>',

                'message'=>'You have a FollowUp at '.$time.' for the job:<b> '.$job->name.'-'.$job->remarks.'</b>',
                'fu_conter'=>$fuconter,
                'followup_date'=> date('Y-m-d 00:00:00', strtotime("$request->followup_date")),
                'remind_me'=>$follodate,
                'date'=>date('Y-m-d', strtotime('-1 day', strtotime($request->followup_date))),
                'admin_status'=>0,
                'user_status'=>0,
                'type'=>1,
                'user_id'=>Auth::user()->id,
                'job_id'=>$jobid
            ];

            Notification::create($data);
        }

        $follow->fu_conter=$fuconter;
        $follow->job_id=$jobid;
        $follow->save();
        $request->session()->flash('alert-success', 'Successful Updated ');




        return redirect('job/'.$jobid.'/edit');

    }


    public function destroy($id)
    {
        //
    }
}
