<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Todo;
use App\UserNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=[
            "1"=>"Next Followup",
            "2"=>"Success",
            "3"=>"Fail",
        ];
        $data=[];

      $select=1;
        $url='todo/add';
        return view('frontend.todo.form',compact(['data','url','categories','select']));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data=$request->all();
        $data['user_id']=Auth::user()->id;
        $data['followup_date']=date('Y-m-d H:i:s', strtotime("$request->followup_date"));
        $todo=Todo::create($data);
        if($request->status==1){
           $job_id=$todo->id;

            $time=date('Y-m-d h:i', strtotime("$request->followup_date"));
            $data=[
                'message'=>'You have a FollowUp at '.$time.'for the job :'.$request->name.'-'.$request->remarks,
                'followup_date'=> date('Y-m-d H:i', strtotime("$request->followup_date")),
                'remind_me'=>date('Y-m-d H:i', strtotime("$request->remind_me")),
                'user_status'=>0,
                'user_id'=>Auth::user()->id,
                'todo_id'=>$job_id
            ];

            UserNotification::create($data);
        }
        $request->session()->flash('alert-success', 'Successful ADDED Job');
        return redirect('todo/view');
    }


    public function show()
    {
        $todos=Todo::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();


        return view('frontend.todo.show',compact(['todos']));
    }


    public function edit($id)
    {
        $categories=[
            "1"=>"Next Followup",
            "2"=>"Success",
            "3"=>"Fail",
        ];

        $data=Todo::findOrfail($id);
        $select=$data->status;

        $url='todo/'.$id.'/update';
        return view('frontend.todo.form',compact(['data','url','categories','select']));
    }


    public function update(Request $request, $id)
    {
        $data=$request->all();
        $data['followup_date']=date('Y-m-d H:i:s', strtotime("$request->followup_date"));
        $todo=Todo::findOrfail($id);
        $todo->update($data);
        if($request->status==1){
            $notify=UserNotification::where('todo_id',$id)->first();
            if($notify)
            {
                $notify->delete();
            }
            $time=date('Y-m-d h:i', strtotime("$request->followup_date"));
            $data=[
                'message'=>'You have a FollowUp at '.$time.'for the job :'.$request->name.'-'.$request->remarks,
                'followup_date'=> date('Y-m-d H:i', strtotime("$request->followup_date")),
                'remind_me'=>date('Y-m-d H:i', strtotime("$request->remind_me")),
                'user_status'=>0,
                'user_id'=>Auth::user()->id,
                'todo_id'=>$id
            ];

            UserNotification::create($data);
        }
        $request->session()->flash('alert-success', 'Successful Updated Job');
        return redirect('todo/view');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo=Todo::findOrfail($id);
        $todo->delete();

        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Successful Deleted');

        return redirect('todo/view');
    }

    public function notify()
    {
        $now=new \DateTime();
        $todos=UserNotification::where([
            'user_id'=>Auth::user()->id,
            'user_status'=>0,


        ])
            ->where('followup_date','>=',($now->format('Y-m-d H:i')))
            ->where('remind_me','<=',($now->format('Y-m-d H:i')))


            ->orderBy('remind_me', 'ASC')

            ->get();

        return view('usernotification',compact('todos'));
    }


    public function header_data()
    {

         $now=new \DateTime();
        $thisdate= date("Y-m-d 00:00:00");
        $user=Auth::user();
        if($user->type==1) {
            $jobs_notification = \App\Notification::where('admin_status', 0)->where(function ($query) use ($thisdate) {
                return $query
                    ->where('date', '=', $thisdate)
                    ->orWhere('followup_date', '=', $thisdate);
            })
                ->where('remind_me', '>=', ($now->format('Y-m-d H:i')))

                ->orderBy('remind_me', 'ASC')

                ->get();

            $todos = new UserNotification();

        }else {
            $jobs_notification = \App\Notification::where([
                'user_id' => \Illuminate\Support\Facades\Auth::user()->id,
                'user_status' => 0,
                'type' => 1,
            ])
                ->where(function ($query) use ($thisdate) {
                    return $query
                        ->where('date', '=', $thisdate)
                        ->orWhere('followup_date', '=', $thisdate);
                })
                ->where('remind_me', '>=', ($now->format('Y-m-d H:i')))
                ->orderBy('remind_me', 'ASC')

                ->get();
            $todos = \App\UserNotification::where([
                'user_id' => \Illuminate\Support\Facades\Auth::user()->id,
                'user_status' => 0,
            ])
                ->where('followup_date', '>=', ($now->format('Y-m-d H:i')))
                ->where('remind_me', '<=', ($now->format('Y-m-d H:i')))
                ->orderBy('remind_me', 'ASC')

                ->get();

        }
        $todos_count=$todos->count();
        $todos=$todos->take(5);
        $jobs_notification_count=$jobs_notification->count();
        $jobs_notification=$jobs_notification->take(5);
        return [
            'job_notification'=>$jobs_notification,
            'todos'=>$todos,
            'todos_count'=>$todos_count,
            'jobs_notification_count'=>$jobs_notification_count


        ];
}
}
