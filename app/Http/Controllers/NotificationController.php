<?php

namespace App\Http\Controllers;

use App\job;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $thisdate= date("Y-m-d 00:00:00");
        $now=new \DateTime();

        if(Auth::user()->type==1){
            $extend='backend.layout';
            $admin=\App\Notification::where('admin_status',0)
                ->where(function($query)use($thisdate){
                return $query
                    ->where('date','=',$thisdate)
                    ->orWhere('followup_date','=',$thisdate);
            })
                ->where('remind_me', '>=', ($now->format('Y-m-d H:i')))

                ->where('type','=',1)

                ->orderBy('remind_me', 'ASC')

                ->get();


            return view('notification',compact('admin','extend','thisdate'));
        }
        $user=\App\Notification::where([
            'user_id'=>\Illuminate\Support\Facades\Auth::user()->id,
            'user_status'=>0,
            'type'=>1
        ])

            ->where(function($query)use($thisdate){
                return $query
                    ->where('date','=',$thisdate)
                    ->orWhere('followup_date','=',$thisdate);
            })
            ->where('remind_me', '>=', ($now->format('Y-m-d H:i')))
            ->orderBy('remind_me', 'ASC')
            ->get();
        $extend='backend.layoutFront';
        return view('notification',compact('user','extend','thisdate'));
    }



}
