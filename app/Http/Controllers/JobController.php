<?php

namespace App\Http\Controllers;

use App\Follow;
use App\job;
use App\Notification;
use App\person;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $text='Add Job';
        $data='';
        $url='job/add';


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c=1;






            $job=new job();
            $person=new person();
            $follow=new Follow();
            $job->name=$request->name;
            $job->address=$request->address;
            $job->landline=$request->landline;
            $job->email=$request->email;
            $job->remarks=$request->remarks;
            $job->user_id=Auth::user()->id;
            $job->save();



        $person->p_name=$request->p_name;
        $person->p_post=$request->p_post;
        $person->p_email=$request->p_email;
        $person->p_mobile=$request->p_mobile;
        $person->fu_conter=$c;
        $person->job_id=$job->id;
        $person->save();
        $follow->f_pitch=$request->f_pitch;
        $follow->p_remarks=$request->p_remarks;
        $follow->f_response=$request->f_response;
        $follow->f_remarks=$request->f_remarks;
        $follow->f_status=$request->f_status;

        if($request->f_status==1){
            $follodate= date('Y-m-d H:i:s', strtotime("$request->followup_date"));

            $follow->followup_date=$follodate;
            $time=date('Y-m-d h:i', strtotime("$request->followup_date"));

            $data=[
                'messageAdmin'=>Auth::user()->name.' have a FollowUp at '.$time.' for the job:<b> '.$job->name.'-'.$job->remarks.'</b>',

                'message'=>'You have a FollowUp at '.$time.' for the job :<b>'.$request->name.'-'.$request->remarks.'</b>',
                'fu_conter'=>$c,
                'followup_date'=> date('Y-m-d 00:00:00', strtotime("$request->followup_date")),
                'remind_me'=>$follodate,
                'date'=>date('Y-m-d', strtotime('-1 day', strtotime($request->followup_date))),
                'admin_status'=>0,
                'user_status'=>0,
                'type'=>1,
                'user_id'=>Auth::user()->id,
                'job_id'=>$job->id
            ];

        Notification::create($data);
        }
        $follow->fu_conter=$c;
        $follow->job_id=$job->id;
        $follow->save();
        $request->session()->flash('alert-success', 'Successful ADDED Job');

        return redirect('job/view');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $datas=job::orderBy('created_at','desc')->get();
        if(Auth::user()->type==1){
            return view('backend.job.show',compact(['datas']));
        }

        return view('frontend.job.show',compact(['datas']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


   $person=person::where('job_id',$id)->groupBy('p_name')->select('id',\DB::raw("concat(p_name,'-',p_post)as name"))->distinct()->lists('name','id');
        $person->prepend('New',0);

        $merges=Follow::join('people','people.fu_conter','=','follows.fu_conter')
            ->where('people.job_id',$id)
            ->where('follows.job_id',$id)
            ->orderBy('people.fu_conter','desc')
        ->get()
        ;


        $job=job::findOrfail($id);

//
//
//
        if(Auth::user()->type==1){
            return view('backend.job.report',compact(['job','merges','person']));
        }
        return view('frontend.job.report',compact(['job','merges','person']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job=job::findOrfail($id);
        $job->delete();

        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Successful Deleted');

        return redirect('admin/job/view');
    }

    public function getPerson(Request $request)
    {
       $id=$request->id;
        $person=person::take(1);
        if($id!=0) {
            $person = person::where('id', $id)->first();
        }
        return [
          'person'=>$person
        ];
    }
}
