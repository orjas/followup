
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-8 wow bounceInLeft animated">
        <div class="sub-title" style="margin-bottom:0;">
          <h4 class="text-left" style="margin-bottom:0; text-align:left;">For Consulting</h4>
        </div>
        <div class="main-title">
          <h3 class="text-left" style="text-align:left !important; font-size: 2em;
margin-top: 10px;">Contact Us</h3>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-widget clearfix"> <span><i class="fa fa-envelope-o"></i> <strong>Email</strong><a href="mailto:{{$setting->email}}" target="_blank">{{$setting->email}}</a> </span> </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-widget clearfix"> <span><i class="fa fa-phone"></i> <strong>Phone</strong><a href="tel:{{$setting->phone}}">{{$setting->phone}}</a> </span> </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-widget clearfix"> <span><i class="fa fa-map-marker"></i> <strong>Address</strong>{{$setting->address}}</span> </div>
          </div>
        </div>
        <div class="row marginTop40">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="main-title2">
              <h3><i class="fa fa-file-text-o" aria-hidden="true"></i> समाचार</h3>
            </div>
            <ul>
              @forelse($samachar as $sama)
              <li><strong style="color: #008131">{{$sama->title}}</strong>
                <p>{!! str_limit($sama->short_description,100) !!}
                <a href="{{route('samachar',['id'=>$sama->id])}}" style="color: #008131" >...continue reading</a>
                </p>
              </li>

              @empty
                <li>No News</li>
              @endforelse
            </ul>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="main-title2">
              <h3><i class="fa fa-file-text-o" aria-hidden="true"></i> ब्लग</h3>
            </div>
            <ul>
              @forelse($blogger as $blog)
                <li><strong style="color: #008131">{{$blog->title}}</strong>
                  <p>{!! str_limit($blog->short_description,100) !!}
                    <a href="{{url('blog/'.$blog->id)}}" style="color: #008131" >...continue reading</a>
                  </p>
                </li>

              @empty
                <li>No Blog</li>
              @endforelse
            </ul>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="main-title2">
              <h3><i class="fa fa-file-text-o" aria-hidden="true"></i> आगमी गतिविधी  </h3>
            </div>
            <ul>
              @forelse($upcomeevents as $up)
                <li><strong style="color: #008131">{{$up->title}}</strong>

                  <p>{!! str_limit($up->short_description,100) !!}
                    <a href="{{route('upcoming')}}" style="color: #008131" >...continue reading</a>
                  </p>
                </li>

              @empty
                <li>No Upcoming Event</li>
              @endforelse
            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 wow bounceInUp animated hidden-sm">
        <div class="facebook-like" style=" margin-top:20px;    padding: 10px;
    background: #404040;
    border: 1px solid #2b2b2b;
">
          <div class="main-title2">
            <h3><span>Like Us On</span> Facebook</h3>
          </div>
          <div class="fb-page" data-href="{{$setting->facebook}}" data-tabs="timeline" data-width="350" data-height="470" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <p>&copy; {{$setting->title}}. All rights reserved</p>
  </div>
</footer>