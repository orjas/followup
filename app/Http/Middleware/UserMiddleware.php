<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->type==2){
                return $next($request);

            }
            else{
                Auth::logout();
                return redirect('c-panel/login');
            }
        }
        else{
            return redirect('c-panel/login');
        }
    }
}
