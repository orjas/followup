<div class="col-md-4 borderLeft wow bounceInRight animated hidden-xs">
          <div class="sidebar">
            <div class="main-title2">
              <h3> <span>Test</span> Prepration</h3>
            </div>
            
            <ul class="sidebar-list">
                      <li><a href="#">TOEFL</a></li>
                      <li><a href="#">IELTS</a></li>
                      <li><a href="#">SAT</a></li>
                      <li><a href="#">GRE</a></li>
                      <li><a href="#">GMAT</a></li>
                      <li><a href="#">PTE (Pearson Test of English)</a></li>
                      <li><a href="#">NAT (Japan)</a></li>
                      <li><a href="#">JLPT(Japan)</a></li>
                    </ul>
          </div>
          
          
          <div class="sidebar top_margin25">
            <div class="main-title2">
              <h3> <span>Study</span> Abroad</h3>
            </div>
            
        <ul class="sidebar-list">
                      <li><a href="#">USA &amp; Canada</a></li>
                      <li><a href="#">Europe</a></li>
                      <li><a href="#">Academic Department</a></li>
                      <li><a href="#">Australia &amp; New Zealand</a></li>
                      <li><a href="#">Study MBBS World Wide</a></li>
                      <li><a href="#">The UK</a></li>
                      <li><a href="#">India</a></li>
                      <li><a href="#">Scholarship &amp; Internship</a></li>
                      <li><a href="#">Engineering &amp; IT</a></li>
                      <li><a href="#">Asia &amp; Others</a></li>
                      <li><a href="#">Japan</a></li>
                    </ul>

          </div>
        </div>