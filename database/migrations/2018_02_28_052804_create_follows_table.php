<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follows', function (Blueprint $table) {
            $table->increments('id');
            $table->text('f_pitch');
            $table->text('p_remarks')->nullable();
            $table->text('f_response');
            $table->string('fu_conter');
            $table->text('f_remarks');
            $table->text('f_status');
            $table->dateTime('followup_date')->nullable();

            $table->integer('job_id')->unsigned();

            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('follows');
    }
}
